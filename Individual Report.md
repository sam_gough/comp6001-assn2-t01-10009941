# Individual Report #

For this project I was nominated as the team leader of the group.

As a group we decided that it would be a good idea to go for a trip out to the Windermere campus to get an idea of the space and also some ideas for the layout as per the new building at the campus that we could relate into our model. 

Tony was tasked with finding a communication channel we could potentially use instead of slack.  
Jeremy suggested a project management tool we could use and so was tasked with looking at it and other ones we could potentially use.  
Regan suggested blender for creating the 3D models for the assets so we tasked himm with looking into that aspect.  
Andrew was tasked into looking at what we should use for producing our wireframes.   
Mark was tasked with looking into the repository system to be used.  
And as I had navigation and the introduction I tasked myself with finding a tool we could use for the floorplan and walls to make them to scale.  

### Floorplan ###

As I was the team leader I tasked myself with looking for a tool we could use to accurately make walls and floors to scale outside of Unity. This is beacuse Unity does not have any form of scaling or standardised measurements.  
I managed to find a free software called [Sketchup](https://www.sketchup.com/). I chose this over the likes of Blender because it is more suited towards architecture which suited the creation of the actual building and it is very simple to use. 

Also becuase I was in charge of the floor plan and we wanted to make the in application building as close to scale as possible I used an image of the floorplan that we found on one of the trips to Windermere. I used google maps to get the dimensions of the outside of the building. I then used this to get some scaled measurements of the inside rooms as best as we could given that we had no architectural plans of the building (you will find the plan with measurements in the slack channel). I also decided to use the plan from the trip as opposed to the one listed in the campus map on their website as that one was less of a representaion of the actual size of the rooms. 

### Navigation ###

As one of my assigned tasks from Jacob was navigation I have been researching how to navigate around the map and have points of interest. One program that I have drawn inspiration from for the navigation is the virtual walkthrough of the valve office that you can access through the SteamVR homepage. This allows the user to walk anywhere around the office but also includes points of interest that have additional information attached to them. This is the form of movement I have proposed to the team. 


